export class LoadData {
    static readonly type = '[Notes] Load Data'

    constructor() {}
}

export class AddNote {
    static readonly type = '[Notes] Add'

    constructor(public payload: Note) {}
}

export class UpdateNote {
    static readonly type = '[Notes] Update'

    constructor(public payload: Note) {}
}

export class RemoveNote {
    static readonly type = '[Notes] Remove'

    constructor(public payload: string) {}
}