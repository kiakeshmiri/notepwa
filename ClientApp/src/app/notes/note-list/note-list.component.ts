import { Component, OnDestroy} from '@angular/core';
import { NotesService } from '../notes.service';
import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material';
import { Store, Select } from '@ngxs/store';
import { NotesState } from 'src/app/state/notes.state';
import { RemoveNote } from '../actions/notes.actions';
import { NoteUpdateDialogComponent } from '../note-update-dialog/note-update-dialog.component';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.scss']
})
export class NoteListComponent implements OnDestroy {

  constructor(public notesService: NotesService, private store: Store, public dialog: MatDialog) { 
    
  }

  @Select(NotesState.selectNotes) notes$: Observable<Note[]>;
  

  openDialog(post: Note): void {
    const dialogRef = this.dialog.open(NoteUpdateDialogComponent, {
      width: '450px',
      data: {_id: post._id, title: post.title, content: post.content }
    });

    dialogRef.afterClosed().subscribe(result => {
      post = result;
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  onEditNote(note: Note) {
    this.openDialog(note);
  }

  onDeleteNote(note: Note) {    
    this.store.dispatch(new RemoveNote(note._id));
  }

}
