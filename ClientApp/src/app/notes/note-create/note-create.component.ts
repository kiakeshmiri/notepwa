import { Component} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AddNote } from '../actions/notes.actions';
import { NotesService } from '../notes.service';

@Component({
  selector: 'app-note-create',
  templateUrl: './note-create.component.html',
  styleUrls: ['./note-create.component.scss']
})
export class NoteCreateComponent {
  enteredTitle = '';
  enteredContent = '';


  constructor(private store: Store, private notesService: NotesService){}


  onAddPost(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.store.dispatch(new AddNote({
      _id: null,
      title: form.value.title,
      content: form.value.content
    }))
    .subscribe(
      (res) => {console.log("note added:"); console.log(res)}
    );
    
    const note: Note = {_id: null, title: form.value.title, content: form.value.content};
    this.notesService.addNote(note);
    form.resetForm();
  }
}
