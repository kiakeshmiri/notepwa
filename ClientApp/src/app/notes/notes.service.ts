import { Injectable } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { ConfigService } from '../services/config.service';
import { BaseService } from '../services/base.service';

@Injectable({
  providedIn: 'root'
})
export class NotesService extends BaseService {

  baseUrl = '';

  constructor(private http: HttpClient, private configService: ConfigService) {
    super();
    this.baseUrl = configService.getApiURI();
  }

  getNotes(): Observable<Note[]> {
    return this.http.get<Note[]>(this.baseUrl + '/notes');
  }

  addNote(note: Note) {
    return this.http.post(this.baseUrl + '/notes', note)
      .pipe(
        catchError(error => this.handleError(error))
      );
  }

  updateNote(note: Note) {
    return this.http.put(this.baseUrl + `/notes/${note._id}`, note)
      .pipe(
        catchError(error => this.handleError(error))
      );

  }

  deleteNote(_id: string) {
    return this.http.delete(this.baseUrl + `/notes/${_id}`)
      .pipe(
        catchError(error => this.handleError(error))
      );
  }
}
