import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NotesService } from '../notes.service';
import { Store } from '@ngxs/store';
import { UpdateNote } from '../actions/notes.actions';

@Component({
  selector: 'app-note-update-dialog',
  templateUrl: './note-update-dialog.component.html',
  styleUrls: ['./note-update-dialog.component.scss']
})
export class NoteUpdateDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<NoteUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Note, private store: Store) { }

  ngOnInit() {
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    this.store.dispatch(new UpdateNote(this.data));    

    this.dialogRef.close();
  }
}
