import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Logout } from '../account/actions/auth.actions';
import { Router } from '@angular/router';
import { PushNotificationService } from '../account/services/push-notification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private store: Store, private router: Router, private notificationService: PushNotificationService) { }

  public isSubscribed$ ;

  ngOnInit() {
    //this.isSubscribed$ = this.notificationService.isSubscribedSubject.asObservable();        
    this.isSubscribed$ = false;        
  }

  Logout() {
    this.store.dispatch(new Logout())
      .subscribe(
        () => {
            this.router.navigate(['/login']);
        }
      );
  }

  SubscribeNotification(){
    this.notificationService.subscribeToNotifications();
  }
}
