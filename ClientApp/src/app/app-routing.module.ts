import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './account/auth.guard';
import { HomeComponent } from './notes/home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent, canActivate: [AuthGuard],
  }];
  
@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
