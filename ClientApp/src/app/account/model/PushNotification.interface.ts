export interface PushNotification{
    endPoint: string;
    p256DH: string;
    auth: string;
}
