import { Credentials } from "../model/Credentials.interface";

export class Login {
    static readonly type = '[Auth] Login';
    constructor(public payload: Credentials) { }
}

export class Logout {
    static readonly type = '[Auth] Logout';
}