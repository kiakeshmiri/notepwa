import { Customer } from "../model/Customer";

export class Register {
    static readonly type = '[Customer] Add'

    constructor(public payload: Customer) {}
}