import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginFormComponent } from './login-form/login-form.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { UserService } from './services/user.service';
import { AccountRoutingModule } from './account.routing';
import { MatComponentsModule } from '../mat-components/mat-components.module';
import { FormsModule } from '@angular/forms';
import { NgxsModule } from '@ngxs/store';
import { AccountState } from './state/account.state';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AccountRoutingModule,
    MatComponentsModule,
    NgxsModule.forFeature([
      AccountState
    ]),

  ],
  declarations: [LoginFormComponent, RegistrationFormComponent],
  providers: [
    UserService,
  ]
})
export class AccountModule { }
