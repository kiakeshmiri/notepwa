import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthState } from './state/auth.state';
import { Store } from '@ngxs/store';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private store: Store, private router: Router) {}

    canActivate() {
        const token = this.store.selectSnapshot(AuthState.token);
    
        if (token === undefined) {
            this.router.navigate(['login']);
            return false;
        }
        return true;
    }
    
}
