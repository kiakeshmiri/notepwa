import { State, Selector, Action, StateContext } from "@ngxs/store";
import { Login, Logout } from "../actions/auth.actions";
import { UserService } from "../services/user.service";
import { tap } from "rxjs/operators";

export class AuthStateModel {
    token?: string;
    username?: string;
}

@State<AuthStateModel>({
    name: 'auth'
})
export class AuthState {

    @Selector()
    static token(state: AuthStateModel) { return state.token; }

    @Selector()
    static username(state: AuthStateModel) { return state.username; }

    constructor(private userService: UserService) { }

    @Action(Login)
    login({ patchState }: StateContext<AuthStateModel>, { payload }: Login) {
        console.log("login data:");
        console.log(payload);
        return this.userService.login(payload)
            .pipe(
                tap((token) => {
                    patchState({ token, username: payload.userName });
                })
            )
    }

    @Action(Logout)
    logout({ setState }: StateContext<AuthStateModel>) {
        this.userService.logout();
        return setState({username: ""});
    }

}
