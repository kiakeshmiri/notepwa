import { Customer } from "../model/Customer";
import { UserService } from "../services/user.service";
import { State, Selector, Action, StateContext } from "@ngxs/store";
import { Register } from "../actions/account.actions";
import { tap } from "rxjs/operators";

export class AccountStateModel {
    customer: Customer;
}

@State<AccountStateModel>({
    name: 'customers',
})

export class AccountState {

    constructor(private userService: UserService) { }

    @Action(Register)
    add({ patchState }: StateContext<AccountStateModel>, { payload }: Register) {
        this.userService.register(payload)
            .subscribe(
                () =>
                    patchState({
                        customer: payload
                    })
            )
    }

}