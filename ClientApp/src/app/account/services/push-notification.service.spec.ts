import { TestBed } from '@angular/core/testing';

import { PushNotificationService } from './push-notification.service';

describe('PushNotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PushNotificationService = TestBed.get(PushNotificationService);
    expect(service).toBeTruthy();
  });

  it('should not be register'), () => {
    const service: PushNotificationService = TestBed.get(PushNotificationService);
    service.subscribeToNotifications();

    expectAsync(service.isSubscribedSubject.toPromise());
  }
});
