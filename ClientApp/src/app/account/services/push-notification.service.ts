import { Injectable } from '@angular/core';
import { SwPush } from '@angular/service-worker';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../../services/config.service';
import { BaseService } from '../../services/base.service';
import { UserService } from './user.service';
import { Store } from '@ngxs/store';
import { AuthState } from '../state/auth.state';
import { PushNotification } from '../model/PushNotification.interface';

@Injectable({
  providedIn: 'root'
})
export class PushNotificationService extends BaseService {

  readonly VAPID_PUBLIC_KEY = "BF5nNYxI4Tuc-FZwgxSC_-s_FQyYBjADzfIKGdsshMPg6eKl_QMgCU1lZCvczdbZxTPcdsM8ViLJ8p-Rh-j0q7A";

  public isSubscribedSubject = new BehaviorSubject<boolean>(false);

  constructor(private swPush: SwPush,
    private httpClient: HttpClient,
    private configService: ConfigService,
    private userService: UserService,
    private store: Store
  ) {
    super();

    const userName = this.store.selectSnapshot(AuthState.username);
    userService.findCustomer(userName)
      .subscribe((customer) => {
        if (customer.subscription != null)
          this.isSubscribedSubject.next(customer.subscription.endpoint != null)
      });
  }

  subscribeToNotifications() {
    if (this.swPush.isEnabled) { //Not active in development

      this.swPush.requestSubscription({
        serverPublicKey: this.VAPID_PUBLIC_KEY
      })
        .then(sub => this.managePushSubscriber(sub))
        .catch(err => console.error("Could not subscribe to notifications", err));
    }
  }

  managePushSubscriber(subscription: any) {
    const endpoint = subscription.endpoint;
    console.log(subscription.getKey('p256dh'));
    const key = btoa(String.fromCharCode.apply(null, new Uint8Array(subscription.getKey('p256dh'))));
    const auth = btoa(String.fromCharCode.apply(null, new Uint8Array(subscription.getKey('auth'))));

    const pSub: PushNotification = { endPoint: endpoint, p256DH: key, auth: auth };

    this.httpClient.put(this.configService._apiURI + `/PushNotification/${this.store.selectSnapshot(AuthState.username)}`, pSub)
      .subscribe(() => console.log("Subscribed!"));
  }
}
