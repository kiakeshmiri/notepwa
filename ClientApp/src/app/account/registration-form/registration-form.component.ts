import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { Customer } from '../model/Customer';
import { UserService } from '../services/user.service';
import { AppUser } from '../model/AppUser';
import { Store } from '@ngxs/store';
import { Register } from '../actions/account.actions';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss']
})
export class RegistrationFormComponent implements OnInit {

  errors: string;
  isRequesting: boolean;
  submitted = false;
  customer: Customer;

  constructor(private userService: UserService, private router: Router, private store: Store) { }

  ngOnInit() {
    this.customer = new Customer();
    this.customer.identity = new AppUser();
    this.isRequesting = false;
  }

  registerUser() {
    this.submitted = true;
    this.isRequesting = true;
    this.errors = '';

    this.store.dispatch(new Register(this.customer))
      .pipe(
        finalize(() => this.isRequesting = false),
      )
      .subscribe(
        result => {
          console.log(result);
          if (result) {
            this.router.navigate(['login'], { queryParams: { brandNew: true, email: this.customer.identity.userName } });
          }
        },
        error => this.errors = error
      );

    // this.userService.register(this.customer)
    //   .pipe(
    //     finalize(() => this.isRequesting = false),
    //   )
    //   .subscribe(
    //     result => {
    //       console.log(result);
    //       if (result) {
    //         this.router.navigate(['login'], { queryParams: { brandNew: true, email: this.customer.identity.userName } });
    //       }
    //     },
    //     error => this.errors = error
    //   );
  }

}
