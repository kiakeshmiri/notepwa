import { State, Selector, Action, StateContext } from '@ngxs/store';
import { AddNote, RemoveNote, LoadData, UpdateNote } from '../notes/actions/notes.actions';
import { tap } from 'rxjs/operators';
import { NotesService } from '../notes/notes.service';

export class NotesStateModel {
    notes: Note[];
}

@State<NotesStateModel>({
    name: 'notes',
    defaults: {
        notes: []
    }
})

export class NotesState {

    constructor(private notesService: NotesService) {
    }

    @Selector()
    static selectNotes(state: NotesStateModel) {
        return state.notes;
    }

    @Action(LoadData)
    public loadData({ patchState }: StateContext<NotesStateModel>) {
        return this.notesService.getNotes()
            .pipe(
                tap(values => patchState({
                    notes: values
                }))
            );
    }

    @Action(AddNote)
    add({ getState, patchState }: StateContext<NotesStateModel>, { payload }: AddNote) {
        this.notesService.addNote(payload)
            .subscribe(() =>
                patchState({
                    notes: [...getState().notes, payload]
                })
            )
    }

    @Action(UpdateNote)
    update({ getState, setState }: StateContext<NotesStateModel>, { payload }: UpdateNote) {

        this.notesService.updateNote(payload)
            .subscribe(() => {
                const state = getState();
                let updateNote = state.notes.find((note) => note._id == payload._id);
                updateNote.content = payload.content;
                updateNote.title = payload.title;

                state.notes[state.notes.findIndex((note) => note._id == payload._id)] = updateNote;

                setState({
                    notes: state.notes
                })

            })
    }

    @Action(RemoveNote)
    remove({ getState, patchState }: StateContext<NotesStateModel>, { payload }: RemoveNote) {
        this.notesService.deleteNote(payload)
            .subscribe(() =>
                patchState({
                    notes: getState().notes.filter(a => a._id != payload)
                })
            )

    }

}