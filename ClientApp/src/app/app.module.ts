import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule, HttpXhrBackend, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { MatComponentsModule } from './mat-components/mat-components.module';
import { AccountModule } from './account/account.module';
import { ConfigService } from './services/config.service';
import { AuthenticateXHRBackend } from './account/authenticate-xhr.backend';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { AuthGuard } from './account/auth.guard';
import { NgxsModule } from '@ngxs/store';
import { NotesState } from './state/notes.state';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { AuthState } from './account/state/auth.state';
import { NoteCreateComponent } from './notes/note-create/note-create.component';
import { NoteListComponent } from './notes/note-list/note-list.component';
import { NoteUpdateDialogComponent } from './notes/note-update-dialog/note-update-dialog.component';
import { HomeComponent } from './notes/home/home.component';
import { PushNotificationService } from './account/services/push-notification.service';

@NgModule({
  declarations: [
    AppComponent,
    NoteCreateComponent,
    HeaderComponent,
    NoteListComponent,
    NoteUpdateDialogComponent,
    HomeComponent
  ],
  entryComponents: [
    NoteUpdateDialogComponent
  ],
  imports: [
    BrowserModule,
    NgxsModule.forRoot([
      AuthState,
      NotesState
    ]),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot(),
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatComponentsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AppRoutingModule,
    AccountModule
  ],
  providers: [
    ConfigService,
    {
      provide: HttpXhrBackend,
      useClass: AuthenticateXHRBackend
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    AuthGuard,
    PushNotificationService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
