import { Component, OnInit } from '@angular/core';
import { SwUpdate, SwPush } from '@angular/service-worker';
import { Store } from '@ngxs/store';
import { LoadData } from './notes/actions/notes.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'Notes';

  update: Boolean = false;

  constructor(updates: SwUpdate, private store: Store) {
    updates.available.subscribe(event => {
      updates.activateUpdate().then(() => document.location.reload());
    });
  }
  
  ngOnInit(): void {
    this.store.dispatch(new LoadData());    
  } 

}
