# Notes PWA
Mongodb, Anular, Dotnetcore, Material Demo and PWA sample


## Purpose

Providing a basic demo on how PWA works.

### Prerequisites
All the installations instructructions works on all supported platforms, However I've personally tested the steps on Windows 10 and Ubuntu 18.x successfully.

The following must be installed (preferably in order) before getting started.

```
Latest Angular (npm install -g @angular/cli).
Latest Angular Material (ng add @angular/material).
Mongodb (refer to  https://www.mongodb.com/download-center/community).
dotnetcore SDK (https://www.microsoft.com/net/download).
your favorite IDE (mine is VS code : https://code.visualstudio.com).

```
There are other packages that automatically will be installed by running npm install.

## Getting Started
After installing prerequisites Clone the repo on your machine and run following commands:

```
cd notes-pwa
npm install
dotnet run
```

It will lunch kestrel hosting notes application and connects to default mongodb port (27017). 


# Angular Web Application

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.2. In addition to that this project is using Angular Material(https://material.angular.io/).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).




