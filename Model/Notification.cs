namespace Model{
    public class Notification{
        public string Title {get; set;}
        public string Body {get; set;}
        public string Icon {get; set;}
    }
}