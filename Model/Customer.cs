using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using WebPush;

namespace Model
{
    public class Customer
    {
        [BsonId(IdGenerator = typeof(ObjectIdGenerator))]
        public ObjectId Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public AppUser Identity { get; set; }  // navigation property
        public string Location { get; set; }
        public string Gender { get; set; }
        public PushSubscription Subscription {get; set;}
    }

    public enum UserCreateResultCode
    {
        Success = 1,
        Failed = 2
    }
}