namespace Model
{
    public class Settings{
        public string Host;
        public string Username;
        public string Password;
        public string Database;
    }
}