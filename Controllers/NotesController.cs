using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model;
using MongoDB.Bson;
using Service;

namespace NotesPWA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private readonly INotesRepository _notesRepository;

        public NotesController(INotesRepository notesRepository)
        {
            _notesRepository = notesRepository;
        }

        // GET: api/posts
        [HttpGet]
        public async Task<IEnumerable<Note>> Get()
        {
            return await _notesRepository.GetAllNotes();
        }

        // GET api/posts/5
        [HttpGet("{id}")]
        public async Task<Note> Get(string id)
        {
            return await _notesRepository.GetNote(id) ?? new Note();
        }


        // POST api/notes
        [HttpPost]
        public async Task<IEnumerable<Note>> Post([FromBody]Note note)
        {
            await _notesRepository.AddNote(new Note()
            { _id = ObjectId.GenerateNewId(), title = note.title, content = note.content });
            return await Get();
        }

        //PUT api/notes/5
        [HttpPut("{id}")]
        public async Task Put(string id, [FromBody]Note note)
        {
            await _notesRepository.UpdateNote(id, note);
        }

        // DELETE api/notes/5
        [HttpDelete("{id}")]
        public async Task Delete(string id)
        {
            await _notesRepository.RemoveNote(id);
        }
    }
}
