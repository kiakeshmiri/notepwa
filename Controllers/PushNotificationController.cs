using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model;
using MongoDB.Bson;
using Service;
using WebPush;
using Newtonsoft.Json;
using ViewModel;

namespace NotesPWA.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PushNotificationController : ControllerBase
    {
        private readonly IUserService userService;
        private WebPushClient pushClient;

        public PushNotificationController(IUserService userService)
        {
            this.userService = userService;
            pushClient = new WebPushClient();
            pushClient.SetVapidDetails("mailto:notes@notesapp.org", "BF5nNYxI4Tuc-FZwgxSC_-s_FQyYBjADzfIKGdsshMPg6eKl_QMgCU1lZCvczdbZxTPcdsM8ViLJ8p-Rh-j0q7A", "8ho_aOvjlhhKBUQcbEPNvp2oq34P4KlNInmJLWVBKdc");
            //{"publicKey":"BF5nNYxI4Tuc-FZwgxSC_-s_FQyYBjADzfIKGdsshMPg6eKl_QMgCU1lZCvczdbZxTPcdsM8ViLJ8p-Rh-j0q7A","privateKey":"8ho_aOvjlhhKBUQcbEPNvp2oq34P4KlNInmJLWVBKdc"}
        }


        [HttpGet("{id}")]
        public string Get(string id)
        {
            string res = "Hello " + id;
            return res;
        }

        [HttpPut("{username}")]
        public async Task<IActionResult> ManageSubscription(string username, [FromBody] PushSubscription subscription)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await userService.FindByNameAsync(username);
            if (result == null)
                return NotFound();

            result.Subscription = subscription;

            try
            {
                await userService.Update(username, result);
                return Ok();
            }
            catch (Exception exp)
            {
                return BadRequest(exp.Message);
            }
        }

        ///
        /*
        Send Notification
         */
        ///
        [HttpPost]
        public async Task<IActionResult> SendNotification([FromBody] NotificationSubscriptionViewModel notification)
        {
            //var notification = new Notification() { Title = "New Notification", Body = "New note Added.", Icon = "assets/icons/favicon-32x32.png" };

            if (notification.UserName != null)
            {
                var user = await userService.FindByNameAsync(notification.UserName);
                if (user.Subscription != null)
                {
                    try
                    {
                        sendNotification(user.Subscription);
                        return Ok();
                    }
                    catch (Exception exp)
                    {
                        return BadRequest(exp.Message);
                    }
                }                
            }
            return NotFound();
        }

        private void sendNotification(PushSubscription subscription)
        {
            WebPushClient wsc = new WebPushClient();

            var notificationPayload = new
            {
                notification = new
                {
                    title = "Angular News",
                    body = "Newsletter Available!",
                    icon = "assets/main-page-logo-small-hat.png",
                    vibrate = new[] { 100, 50, 100 },
                    data = new[]{
                        new {
                            dateOfArrival = DateTime.Now,
                            primaryKey = 1
                        }
                    },
                    actions = new[] {
                        new {
                            action = "explore",
                            title = "Go to the site"
                        }
                    }
                }
            };
            var payload = JsonConvert.SerializeObject(notificationPayload);

            try
            {
                wsc.SendNotification(subscription, payload);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }
    }
}