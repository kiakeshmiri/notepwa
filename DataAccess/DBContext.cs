using System;
using System.Collections.Generic;
using MongoDB.Driver;
using Microsoft.Extensions.Options;
using Model;


namespace DataAccess
{
    public class DBContext
    {
        private readonly IMongoDatabase _database = null;

        public DBContext()
        {
          var client = new MongoClient("mongodb://localhost:27017");
          _database = client.GetDatabase("notesDb");

        }

        public IMongoCollection<Note> Notes
        {
            get
            {
                return _database.GetCollection<Note>("note");
            }
        }
	
	public IMongoCollection<Customer> Customers
        {
            get
            {
                return _database.GetCollection<Customer>("customer");
            }
        }
    }
}
