using System;
using Microsoft.Extensions.Options;
using Model;
using System.Threading.Tasks;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using DataAccess;

namespace Service
{
    public class NotesRepository : INotesRepository
    {
        private readonly DBContext _context = null;

        public NotesRepository()
        {
            _context = new DBContext();
        }

        public async Task<IEnumerable<Note>> GetAllNotes()
        {
            try
            {
                return await _context.Notes
                        .Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<Note> GetNote(string id)
        {
            var filter = Builders<Note>.Filter.Eq("_id", id);

            try
            {
                return await _context.Notes
                                .Find(filter)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task AddNote(Note item)
        {
            try
            {
                await _context.Notes.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
        public async Task UpdateNote(string id, Note item)
        {
            try
            {
                ObjectId objectId = ObjectId.Parse(id);
                var filter = Builders<Note>.Filter.Eq("_id", objectId);
                var update = Builders<Note>.Update.Set(x => x.title, item.title).Set("content", item.content);
                var result = await _context.Notes.UpdateOneAsync(filter, update);  
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }


        public async Task RemoveNote(string id)
        {
            try
            {                
                ObjectId objectId = ObjectId.Parse(id);
                await _context.Notes.DeleteOneAsync(Builders<Note>.Filter.Eq("_id", objectId));
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
    }
}
