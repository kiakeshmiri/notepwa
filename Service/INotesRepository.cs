using System.Collections.Generic;
using System.Threading.Tasks;
using Model;

namespace Service
{
    public interface INotesRepository
    {
        Task<IEnumerable<Note>> GetAllNotes();
        Task<Note> GetNote(string id);
        Task AddNote(Note item);
        Task UpdateNote(string id, Note item);
        Task RemoveNote(string id);
    }
}
