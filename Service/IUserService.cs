using System.Threading.Tasks; 
using System.Collections.Generic;  

using Model;

namespace Service
{
    public interface IUserService
    {
        Task<UserCreateResultCode> Create(Customer customer);
        Task<Customer> FindByNameAsync(string userName);
        Task<Customer> FindByUserIdAsync(string userId);
        Task Update(string username, Customer customer);
        bool CheckPasswordAsync(Customer customer, string password);
    }
}