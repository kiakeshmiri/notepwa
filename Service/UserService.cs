using System;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;

using DataAccess;
using Model;


namespace Service
{
    public class UserService : IUserService
    {
        private readonly DBContext _context = null;

        public UserService()
        {
            _context = new DBContext();
        }
        public async Task<UserCreateResultCode> Create(Customer customer)
        {
            try
            {
                await _context.Customers.InsertOneAsync(customer);
                return UserCreateResultCode.Success;
            }
            catch (Exception exp)
            {
                System.Console.WriteLine(exp.Message);
                // log or manage the exception
                return UserCreateResultCode.Failed;
            }
        }

        public async Task<Customer> FindByNameAsync(string userName)
        {
            return await _context.Customers
                .Find(u => u.Identity.UserName == userName)
                .FirstOrDefaultAsync();
        }
        public async Task<Customer> FindByUserIdAsync(string userId)
        {
            var stringFilter = "{ _id: ObjectId('" + userId + "') }";
            return await _context.Customers
                .Find(stringFilter)
                .FirstOrDefaultAsync();
        }
        public async Task<List<Customer>> FindAll(string userId)
        {
            return await _context.Customers.Find(_ => true).ToListAsync();
        }
        public bool CheckPasswordAsync(Customer customer, string password)
        {
            var exists = customer.Identity.UserName == customer.Identity.UserName && customer.Identity.Password == password;
            return exists;
        }

        public async Task Update(string userName, Customer customer)
        {
            Customer updateCustomer = await FindByNameAsync(userName);
            updateCustomer.FirstName = customer.FirstName;
            updateCustomer.LastName = customer.LastName;
            updateCustomer.Location = customer.Location;
            updateCustomer.Subscription = customer.Subscription;

            var updateDef = Builders<Customer>.Update
                .Set(c => c.FirstName, customer.FirstName)
                .Set(c => c.LastName, customer.LastName)
                .Set(c => c.Location, customer.Location)
                .Set(c => c.Subscription, customer.Subscription);

            var result = _context.Customers
                    .UpdateOneAsync(o => o.Identity.UserName == userName, updateDef);

            if (!result.IsCompletedSuccessfully)
                throw new Exception("Update Failed!");
        }

    }
}